# About avc-commons3-query-tests

Tests against the **avc-commons3-query**
framework.

[API Documentation is here](https://maven.avcompris.com/avc-commons3-query-tests/apidocs/index.html).

There is a [Maven Generated Site.](https://maven.avcompris.com/avc-commons3-query-tests/)
