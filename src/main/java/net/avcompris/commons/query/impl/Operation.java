package net.avcompris.commons.query.impl;

import static net.avcompris.commons.query.impl.FieldUtils.isBooleanField;
import static net.avcompris.commons.query.impl.FieldUtils.isDateTimeField;
import static net.avcompris.commons.query.impl.FieldUtils.isEnumField;
import static net.avcompris.commons.query.impl.FieldUtils.isIntField;
import static net.avcompris.commons.query.impl.FieldUtils.isStringField;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;

import net.avcompris.commons.query.Filtering.Field;

public enum Operation {

	EQ("="),

	NEQ("!="),

	CONTAINS(null),

	DOESNT_CONTAIN(null),

	LT("<"),

	LTE("<="),

	GT(">"),

	GTE(">=");

	@Nullable
	private final String arithmeticOperator;

	Operation(@Nullable final String arithmeticOperator) {

		this.arithmeticOperator = arithmeticOperator;
	}

	@Nullable
	public String arithmeticOperator() {

		return arithmeticOperator;
	}

	public static Operation[] getOperations(final Field field) {

		if (isStringField(field)) {

			return new Operation[] { EQ, NEQ, CONTAINS, DOESNT_CONTAIN };

		} else if (isIntField(field)) {

			return new Operation[] { EQ, NEQ, GT, GTE, LT, LTE };

		} else if (isBooleanField(field)) {

			return new Operation[] { EQ, NEQ };

		} else if (isDateTimeField(field)) {

			return new Operation[] { EQ, NEQ, GT, GTE, LT, LTE };

		} else if (isEnumField(field)) {

			return new Operation[] { EQ, NEQ };

		} else {

			throw new NotImplementedException("field: " + field);
		}
	}
}
