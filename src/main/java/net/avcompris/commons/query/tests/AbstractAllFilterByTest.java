package net.avcompris.commons.query.tests;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons.query.impl.FieldUtils.extractPropertyName;
import static net.avcompris.commons.query.impl.FieldUtils.isBooleanField;
import static net.avcompris.commons.query.impl.FilteringsFactory.instantiate;
import static org.apache.commons.lang3.StringUtils.capitalize;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import net.avcompris.commons.query.FilterSyntaxException;
import net.avcompris.commons.query.Filtering;
import net.avcompris.commons.query.Filterings;
import net.avcompris.commons.query.impl.Operation;

public abstract class AbstractAllFilterByTest<T extends Filtering<U>, U extends Filtering.Field> {

	private final Class<?> dtoClass;
	// private final String expression;
	// private final U field;
	// private final Class<? extends Filterings<T, U>> filteringsClass;
	private final Filterings<T, U> filterings;

//	private final Class<U> contentHandlerClass;

	protected AbstractAllFilterByTest(final Class<? extends Filterings<T, U>> filteringsClass,
			final Class<?> dtoClass) {

		// this.filteringsClass =
		checkNotNull(filteringsClass, "filteringsClass");
		this.dtoClass = checkNotNull(dtoClass, "dtoClass");
		// this.contentHandlerClass = contentHandlerClass;
		// this.expression = checkNotNull(expression, "expression");
		// this.field = checkNotNull(field, "field");

		filterings = instantiate(filteringsClass);

		checkArgument(dtoClass.isInterface(), //
				"dtoClass should be an interface: %s", dtoClass.getName());
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("testCases")
	public final void testParse(final String expression, final U field) throws Exception {

		final String s = expression + " " + newValueFor(field);

		parse(s);
	}

	/*
	 * private static String capitalize(final String name) {
	 * 
	 * final StringBuilder sb = new StringBuilder();
	 * 
	 * boolean start = true;
	 * 
	 * for (final char c : name.toCharArray()) {
	 * 
	 * if (c == '_') {
	 * 
	 * start = true;
	 * 
	 * } else if (start) {
	 * 
	 * sb.append(c);
	 * 
	 * start = false;
	 * 
	 * } else {
	 * 
	 * sb.append(Character.toLowerCase(c)); } }
	 * 
	 * return sb.toString(); }
	 */

	@ParameterizedTest(name = "{0}")
	@MethodSource("testCases")
	public final void testMatch(final String expression, final U field) throws Exception {

		final T filtering = filterings.parse(expression + " " + newValueFor(field));

		// final Method match = extractMatchMethod(filtering.getClass());

		// final Class<?> contentHandlerClass = match.getParameterTypes()[0];

		final String prefix;

		if (isBooleanField(field)) {

			prefix = "is";

		} else {

			prefix = "get";
		}

		final String propertyName = extractPropertyName(field);

		final String getterMethodName = prefix + capitalize(propertyName);

		final Object p = Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
				new Class<?>[] { dtoClass }, new InvocationHandler() {

					@Override
					public Object invoke(final Object proxy, final Method method, final Object[] args)
							throws Throwable {

						final String expectedMethodName = method.getReturnType().isArray() //
								? getterMethodName + "s" //
								: getterMethodName;

						if (!expectedMethodName.contentEquals(method.getName())) {
							throw new IllegalStateException(
									"method: " + method + ", expectedMethodName: " + expectedMethodName);
						}

						return randomValueFor(field);
					}
				});

		filtering.match(p);
	}

	protected abstract Object newValueFor(U field);

	protected final Object randomValueFor(final U field) {

		return newValueFor(field);
	}

	protected final T parse(final String expression) throws FilterSyntaxException {

		return filterings.parse(expression);
	}

	protected static <U extends Filtering.Field> Stream<Arguments> testCases(final U[] values) throws Exception {

		return parameters(values).stream() //
				.map((args) -> Arguments.of(args[0], args[1]));
	}

	private static <U extends Filtering.Field> Collection<Object[]> parameters(final U[] values) throws Exception {

		final List<Object[]> parameters = new ArrayList<>();

		for (final U field : values) {

			for (final Operation operation : Operation.getOperations(field)) {

				final String expression = field + " " + operation;

				parameters.add(new Object[] { expression, field });
			}
		}

		return parameters;
	}
}
